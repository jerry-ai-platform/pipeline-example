import kfp.dsl as dsl
from kfp.dsl import PipelineVolume

# To compile the pipeline:
#   dsl-compile --py pipeline.py --output pipeline.tar.gz
from constants import PROJECT_ROOT, CONDA_PYTHON_CMD, BUCKET_NAME


@dsl.pipeline(
    name='Mnist reaining',
    description='MNIST Training Pipeline to be executed on KubeFlow.'
)
def train_pipeline_cnn(images: str = 'kerwintsaiii/kubeflow-mnist:v1.0.3-gpu',
                        bucket_name: str = 'mnist',
                        batch_size: str = '128',
                        epoch: str = '30'):

    run_bucket_name = str(bucket_name) + '-' + str(BUCKET_NAME)
    
    commands_pre = [f"cd {PROJECT_ROOT}", 
                f"git pull && git checkout gpu",
                f"python3 preprocessing.py --data_dir dataset --bucket_name {run_bucket_name}"]
    
    commands_train = [f"cd {PROJECT_ROOT}", 
                f"git pull && git checkout gpu",
                f"python3 train.py --data_dir dataset --model_path save_model --bucket_name {run_bucket_name} --batch-size {batch_size} --epoch {epoch}"]

    preprocessing = dsl.ContainerOp(
        name='preprocessing',
        image=images,
        command=['sh'],
        arguments=[
            '-c', ' && '.join(commands_pre),
        ],
    )

    train = dsl.ContainerOp(
        name='train',
        image=images,
        command=['sh'],
        arguments=[
            '-c', ' && '.join(commands_train),
        ],
        file_outputs={'model': PROJECT_ROOT + "/save_model/saved_model.h5"}
    )
    train.set_gpu_limit('1')
    train.after(preprocessing)

    serve_args = [
        '--model-export-path', "",
        '--server-name', "mnist-service"
    ]
    serve = dsl.ContainerOp(
      name='serve',
      image='gcr.io/ml-pipeline/ml-pipeline-kubeflow-deployer:'
            '7775692adf28d6f79098e76e839986c9ee55dd61',
      arguments=serve_args
    )
    serve.after(train)

    webui_args = [
            '--image', 'gcr.io/kubeflow-examples/mnist/web-ui:'
                        'v20190304-v0.2-176-g15d997b-pipelines',
            '--name', 'web-ui',
            '--container-port', '5000',
            '--service-port', '80',
            '--service-type', "LoadBalancer"
    ]
    web_ui = dsl.ContainerOp(
        name='web-ui',
        image='gcr.io/kubeflow-examples/mnist/deploy-service:latest',
        arguments=webui_args
    )
    web_ui.after(serve)

if __name__ == '__main__':
    import kfp.compiler as compiler
    compiler.Compiler().compile(train_pipeline_cnn, __file__ + '.tar.gz')
