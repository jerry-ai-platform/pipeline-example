import os
from datetime import datetime
import random
import string

PROJECT_ROOT = os.path.join('/', 'workspace')
BUCKET_NAME = ''.join(random.choice(string.ascii_lowercase) for i in range(10)) + '-' + str(datetime.now().strftime("%m%d%H%M%S"))
CONDA_PYTHON_CMD = '/opt/conda/envs/kubeflow-mnist/bin/python'
